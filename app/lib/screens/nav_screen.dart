//  Own
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Const
import '../util/consts.dart';
//  Providers
import 'package:app/providers/auth.dart';
import 'package:app/providers/internetConnection.dart';
import 'package:app/providers/screens.dart';
import '../providers/transactions.dart';
import '../providers/screens.dart';
//  Widgets

class NavScreen extends StatefulWidget {
  @override
  _NavScreenState createState() => _NavScreenState();
}

class _NavScreenState extends State<NavScreen> {
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Transactions>(context, listen: false)
          .initialFetchTransactionsOfUser(
              Provider.of<Auth>(context, listen: false).getUserID)
          .then((connectionState) {
        if (connectionState) {
          Provider.of<InternetConnection>(context, listen: false)
              .updateConnection(CONNECTION.CONNECTED);
        } else {
          Provider.of<InternetConnection>(context, listen: false)
              .updateConnection(CONNECTION.NOT_CONNECTED);
        }
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    //  Screens Provider
    final screensProvider = Provider.of<Screens>(context);

    return Consumer<Screens>(
      builder: (context, pages, __) => Scaffold(
        backgroundColor: AppColors.primary,
        appBar: AppBar(
          title: Center(
            child: Text('Expenses App'),
          ),
          bottomOpacity: 0.0,
          elevation: 0.0,
          backgroundColor: AppColors.primary,
        ),
        body: _isLoading
            ? Center(
                child: Container(
                  child: CircularProgressIndicator(),
                ),
              )
            : screensProvider.currentScreen.child,
        bottomNavigationBar: BottomNavigationBar(
          onTap: screensProvider.setTab,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.attach_money),
              label: 'Financial',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.bar_chart),
              label: 'Statistics',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.room),
              label: 'Near ATMs',
            ),
          ],
          backgroundColor: AppColors.primary,
          unselectedItemColor: AppColors.light1,
          selectedItemColor: AppColors.light1,
          type: BottomNavigationBarType.fixed,
          iconSize: 30,
        ),
      ),
    );
  }
}
