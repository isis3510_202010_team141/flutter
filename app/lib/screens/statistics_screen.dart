import 'package:app/util/consts.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class StatisticsScreen extends StatelessWidget {
  static const routeName = '/stats';
  String numb;

  StatisticsScreen() {
    _getRecommendation();
  }

  Future<void> _getRecommendation() {
    const url = 'https://django-middle-finance.herokuapp.com/statistics/1';
    http.get(url, headers: {'Content-Type': 'application/json'}).then(
        (response) {
      var resp = json.decode(response.body);
      numb = double.parse(resp['value']).toStringAsFixed(2);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: AppBar(
        title: Text('Statistics'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: RefreshIndicator(
        onRefresh: () => _getRecommendation(),
        child: Center(
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(100),
            height: double.infinity,
            color: AppColors.secondary,
            child: Text(
              'We calculated a Maximum expenditures for today. Try do not Spend more than: $numb.',
              style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 25,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
