import 'package:flutter/material.dart';

import 'package:app/util/consts.dart';
import 'package:provider/provider.dart';
import '../providers/transaction.dart';
import '../providers/transactions.dart';
import '../widgets/submit_button.dart';
import '../providers/auth.dart';

class NewTransactionScreen extends StatefulWidget {
  static const routeName = '/new-transaction';

  @override
  _NewTransactionScreenState createState() => _NewTransactionScreenState();
}

class _NewTransactionScreenState extends State<NewTransactionScreen> {
  final _nameFocusNode = FocusNode();
  final _valueFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();

  Transaction _createdTransaction = Transaction(
    id: null,
    userId: null,
    name: '',
    type: 1,
    value: 0,
    date: DateTime.now(),
    uploaded: null,
  );

  void dispose() {
    _nameFocusNode.dispose();
    _valueFocusNode.dispose();
    super.dispose();
  }

  void _saveForm() {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    Provider.of<Transactions>(context, listen: false)
        .addTransaction(_createdTransaction);
    Navigator.of(context).pop();
  }

  void setType(int val) {
    setState(() {
      _createdTransaction = Transaction(
        id: _createdTransaction.id,
        userId: _createdTransaction.userId,
        name: _createdTransaction.name,
        type: val,
        value: _createdTransaction.value,
        date: _createdTransaction.date,
        uploaded: _createdTransaction.uploaded,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Transaction'),
      ),
      body: Container(
        color: AppColors.secondary,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Form(
            key: _form,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Transaction Name',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 24,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: TextFormField(
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: InputDecoration(
                            labelText: 'Name',
                            labelStyle: TextStyle(
                              color: Theme.of(context).accentColor,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.0,
                                  color: Theme.of(context).accentColor),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 3.0,
                                  color: Theme.of(context).accentColor),
                            ),
                          ),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          focusNode: _nameFocusNode,
                          onSaved: (val) {
                            _createdTransaction = Transaction(
                              id: _createdTransaction.id,
                              userId: Provider.of<Auth>(context, listen: false)
                                  .getUserID,
                              name: val,
                              type: _createdTransaction.type,
                              value: _createdTransaction.value,
                              date: _createdTransaction.date,
                              uploaded: _createdTransaction.uploaded,
                            );
                          },
                          validator: (val) {
                            if (val.isEmpty) {
                              return 'Please provide a Name';
                            }
                            return null;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Transaction Type',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 24,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Radio(
                                value: 0,
                                groupValue: _createdTransaction.type,
                                onChanged: (val) => setType(val),
                                activeColor: Theme.of(context).accentColor,
                              ),
                              Text(
                                'Income',
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 24),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                value: 1,
                                groupValue: _createdTransaction.type,
                                onChanged: (val) => setType(val),
                                activeColor: Theme.of(context).accentColor,
                              ),
                              Text(
                                'Outcome',
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 24),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Transaction Value',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontSize: 24,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: TextFormField(
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                          ),
                          decoration: InputDecoration(
                            labelText: 'Value',
                            labelStyle: TextStyle(
                              color: Theme.of(context).accentColor,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 1.0,
                                  color: Theme.of(context).accentColor),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 3.0,
                                  color: Theme.of(context).accentColor),
                            ),
                          ),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          focusNode: _valueFocusNode,
                          onSaved: (val) {
                            _createdTransaction = Transaction(
                              id: _createdTransaction.id,
                              userId: Provider.of<Auth>(context, listen: false)
                                  .getUserID,
                              name: _createdTransaction.name,
                              type: _createdTransaction.type,
                              value: double.parse(val),
                              date: _createdTransaction.date,
                              uploaded: _createdTransaction.uploaded,
                            );
                          },
                          validator: (val) {
                            if (val.isEmpty) {
                              return 'Please provide a Value';
                            } else if (double.tryParse(val) == null) {
                              return 'Please provide a valid Value';
                            } else if (double.parse(val) <= 0) {
                              return 'Please provide a number greater than zero';
                            }
                            return null;
                          },
                        ),
                      )
                    ],
                  ),
                ),
                SubmitButton('Submit', () {
                  final userId =
                      Provider.of<Auth>(context, listen: false).getUserID;
                  _createdTransaction = Transaction(
                    id: _createdTransaction.id,
                    userId: userId,
                    name: _createdTransaction.name,
                    type: _createdTransaction.type,
                    value: _createdTransaction.value,
                    date: _createdTransaction.date,
                    uploaded: _createdTransaction.uploaded,
                  );
                  _saveForm();
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
