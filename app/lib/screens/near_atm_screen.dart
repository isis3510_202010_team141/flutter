import 'package:app/providers/atms.dart';
import 'package:app/widgets/atms_grid.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Const
import '../util/consts.dart';

class NearATMsScreen extends StatelessWidget {
  static const routeName = '/atms';

  Future<void> _refresh(BuildContext context) async {
    await Provider.of<ATMs>(context, listen: false).getNearATMs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: AppBar(
        title: Center(
          child: Text('Near ATMs to you'),
        ),
        bottomOpacity: 0.0,
        elevation: 0.0,
        backgroundColor: AppColors.primary,
        actions: [
          IconButton(
            icon: const Icon(Icons.replay_outlined),
            onPressed: () {
              Provider.of<ATMs>(context, listen: false).getNearATMs();
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => _refresh(context),
        child: Container(
          width: double.infinity,
          color: AppColors.secondary,
          child: ATMsGrid(),
        ),
      ),
    );
  }
}
