import 'package:app/providers/auth.dart';
import 'package:app/util/consts.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/home';
  @override
  Widget build(BuildContext context) {
    final userId = Provider.of<Auth>(context).getUserID;
    final userName = Provider.of<Auth>(context).getUserName;
    final userAge = Provider.of<Auth>(context).getUserAge;
    final userEmail = Provider.of<Auth>(context).getUserEmail;
    final userPhone = Provider.of<Auth>(context).getUserPhone;
    //  This is the Screen Size
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: AppColors.primary,
        child: Container(
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(vertical: 30),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
            ),
            color: AppColors.secondary,
            boxShadow: [
              BoxShadow(
                color: AppColors.secondary,
                offset: Offset(0, -5),
                blurRadius: 0.0,
              )
            ],
          ),
          child: Center(
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Card(
                    margin: EdgeInsets.all(20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    elevation: 8.0,
                    child: Container(
                      padding: EdgeInsets.all(30),
                      height: 450,
                      constraints: BoxConstraints(minHeight: 450),
                      width: deviceSize.width * 0.75,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        color: AppColors.light1,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            blurRadius: 10.0,
                            spreadRadius: 2.0,
                            offset: Offset(0.0, 3.0),
                          )
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          //  Profile Image
                          Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(
                                  'https://cataas.com/cat',
                                ),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black54,
                                  blurRadius: 5.0,
                                  spreadRadius: 0.0,
                                  offset: Offset(1.0, 1.0),
                                )
                              ],
                            ),
                          ),
                          //  Items
                          Expanded(
                            child: DataTable(columns: [
                              DataColumn(label: Text('')),
                              DataColumn(label: Text('')),
                            ], rows: [
                              DataRow(
                                cells: [
                                  DataCell(Text('ID:')),
                                  DataCell(Text('$userId')),
                                ],
                              ),
                              DataRow(
                                cells: [
                                  DataCell(Text('Name:')),
                                  DataCell(Text('$userName')),
                                ],
                              ),
                              DataRow(
                                cells: [
                                  DataCell(Text('Age:')),
                                  DataCell(Text('$userAge')),
                                ],
                              ),
                              DataRow(
                                cells: [
                                  DataCell(Text('Email:')),
                                  DataCell(Text('$userEmail')),
                                ],
                              ),
                              DataRow(
                                cells: [
                                  DataCell(Text('Phone:')),
                                  DataCell(Text('$userPhone')),
                                ],
                              ),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Provider.of<Auth>(context, listen: false).logout();
        },
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.exit_to_app_outlined, color: AppColors.light1),
      ),
    );
  }
}
