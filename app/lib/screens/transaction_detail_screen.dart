import 'package:app/providers/transactions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TransactionDetailScreen extends StatelessWidget {
  static const routeName = '/transaction-detail';

  @override
  Widget build(BuildContext context) {
    final transactionId = ModalRoute.of(context).settings.arguments as int;
    return Scaffold(
      appBar: AppBar(
        title: Text(Provider.of<Transactions>(context, listen: false)
            .findById(transactionId)
            .name),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
