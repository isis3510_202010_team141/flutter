//  Own
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Const
import 'package:app/util/consts.dart';
//  Widgets
import '../widgets/transactions_grid.dart';
//  Providers
import '../providers/transactions.dart';
import '../providers/auth.dart';
import '../providers/internetConnection.dart';

class TransactionsOverviewScreen extends StatelessWidget {
  Future<void> _refreshTransactions(BuildContext context) async {
    final userId = Provider.of<Auth>(context, listen: false).getUserID;
    final connectionState =
        await Provider.of<Transactions>(context, listen: false)
            .initialFetchTransactionsOfUser(userId);
    if (connectionState) {
      Provider.of<InternetConnection>(context, listen: false)
          .updateConnection(CONNECTION.CONNECTED);
    } else {
      Provider.of<InternetConnection>(context, listen: false)
          .updateConnection(CONNECTION.NOT_CONNECTED);
    }
  }

  static const routeName = '/transactions';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: RefreshIndicator(
        onRefresh: () => _refreshTransactions(context),
        child: Container(
          child: Container(
            margin: EdgeInsets.only(top: 10),
            child: TransactionsGrid(),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
              color: AppColors.secondary,
              boxShadow: [
                BoxShadow(
                  color: AppColors.secondary,
                  offset: Offset(0, -5),
                  blurRadius: 0.0,
                )
              ],
            ),
          ),
          color: AppColors.primary,
        ),
      ),
    );
  }
}
