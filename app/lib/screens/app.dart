//  Own
import 'package:app/screens/nav_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Providers
import 'package:app/providers/screens.dart';
import '../providers/screens.dart';

class FinancesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        //  This manages all the Screens data.
        ChangeNotifierProvider.value(
          value: Screens(),
        ),
      ],
      child: NavScreen(),
    );
  }
}
