import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class TransactionsDAO {
  //
  //
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    //await sql.deleteDatabase(path.join(dbPath, 'finances.db'));

    //  While
    //await sql.deleteDatabase(path.join(dbPath, 'finances.db'));

    return sql.openDatabase(path.join(dbPath, 'finances.db'),
        onCreate: (db, version) {
      return db.execute(
        'CREATE TABLE transactions(id INTEGER PRIMARY KEY, userId INTEGER, name TEXT, type INTEGER, value REAL, date REAL, uploaded INTEGER)',
      );
    }, version: 1);
  }

  static Future<void> insertTransaction(Map<String, Object> data) async {
    final db = await database();
    db.insert(
      'transactions',
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getTransactions(int userId) async {
    final db = await database();
    return db.rawQuery(
        'SELECT * FROM transactions WHERE userId=$userId ORDER BY date desc');
  }

  static Future<List<Map<String, dynamic>>> getNotSaved(int userId) async {
    final db = await database();
    return db.rawQuery(
        'SELECT * FROM transactions WHERE uploaded=0 AND userId=$userId ORDER BY date desc');
  }

  static Future<List<Map<String, dynamic>>> getSaved(int userId) async {
    final db = await database();
    return db.rawQuery(
        'SELECT * FROM transactions WHERE uploaded=1 AND userId=$userId ORDER BY date desc');
  }

  static Future<void> removeNotSaved(int userId) async {
    final db = await database();
    db.rawQuery('DELETE FROM transactions WHERE uploaded=0 and userId=$userId');
  }

  static Future<void> removeSaved(int userId) async {
    final db = await database();
    db.rawQuery('DELETE FROM transactions WHERE uploaded=1 and userId=$userId');
  }

  static Future<void> removeAll(int userId) async {
    final db = await database();
    db.rawQuery('DELETE FROM transactions WHERE userId=$userId');
  }

/*
  static Future<List<Map<String, dynamic>>> dropTransactions() async {
    final db = await database();
    await db.execute("DROP TABLE IF EXISTS transactions");
    await db.execute(
        'CREATE TABLE transactions(id INTEGER PRIMARY KEY, userId INTEGER, name TEXT, type INTEGER, value REAL, date REAL, uploaded INTEGER)');
  }
  */
}
