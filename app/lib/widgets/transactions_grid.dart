import 'package:app/providers/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../util/consts.dart';
import '../providers/transactions.dart';
import './transaction_item.dart';
import 'transaction_item.dart';
import '../providers/internetConnection.dart';
import '../screens/new_transactions.dart';

class TransactionsGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userId = Provider.of<Auth>(context).getUserID;
    final transactionsData = Provider.of<Transactions>(context);
    final transactions = transactionsData.items;
    final connectionStatus = Provider.of<InternetConnection>(context);

    return transactionsData.isLoadingInit
        ? Center(
            child: CircularProgressIndicator(),
          )
        : transactions.isEmpty
            ? Center(
                child: Text(
                  'You have no transactions yet, add one using the "Add" button',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.light1,
                  ),
                ),
              )
            : CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: connectionStatus.getConnection ==
                            CONNECTION.NOT_CONNECTED
                        ? Container(
                            child: Center(
                              child: Text('You are not connected!'),
                            ),
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50),
                                topRight: Radius.circular(50),
                              ),
                              color: AppColors.outcome,
                            ),
                          )
                        : Container(),
                  ),
                  SliverToBoxAdapter(
                    child: transactionsData.isCreating
                        ? Container(
                            child: Row(
                              children: [
                                Text(
                                  'Adding',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: AppColors.light1,
                                  ),
                                ),
                                Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                            ),
                            margin: EdgeInsets.all(20),
                          )
                        : Container(
                            child: Row(
                              children: [
                                Text(
                                  'My Transactions',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: AppColors.light1,
                                  ),
                                ),
                                OutlineButton(
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(
                                      NewTransactionScreen.routeName,
                                    );
                                  },
                                  child: Text(
                                    'Add',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: AppColors.light1,
                                    ),
                                  ),
                                  borderSide: BorderSide(
                                    width: 1.0,
                                    color: AppColors.light1,
                                  ),
                                  padding: EdgeInsets.all(10),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                            ),
                            padding: EdgeInsets.symmetric(vertical: 10),
                          ),
                  ),
                  SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                      (ctx, i) => ChangeNotifierProvider.value(
                        value: transactions[i],
                        child: TransactionItem(),
                      ),
                      childCount: transactions.length,
                    ),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      childAspectRatio: 3 / 1,
                      mainAxisSpacing: 10,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      child: Center(
                        child: transactionsData.isLoading
                            ? CircularProgressIndicator()
                            : Text('No more transactions to show.'),
                      ),
                      margin: EdgeInsets.all(30),
                    ),
                  )
                ],
                controller: transactionsData.initScroll(userId),
              );
  }
}
