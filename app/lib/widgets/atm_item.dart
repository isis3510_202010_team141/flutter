import 'package:app/providers/atm.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class ATMItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final atm = Provider.of<ATM>(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: Image.network(
          atm.photo,
          fit: BoxFit.cover,
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          title: Column(children: [
            Text(atm.name, textAlign: TextAlign.center),
            Text(atm.address, textAlign: TextAlign.center)
          ]),
        ),
      ),
    );
  }
}
