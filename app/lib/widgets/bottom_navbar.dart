//  Own
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Const
import 'package:app/util/consts.dart';
//  providers
import 'package:app/providers/screens.dart';

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final change = Provider.of<Screens>(context).setTab;

    return BottomNavigationBar(
      onTap: (index) => change(index),
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.attach_money),
          label: 'Financial',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.bar_chart),
          label: 'Statistics',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.room),
          label: 'Near ATMs',
        ),
      ],
      backgroundColor: AppColors.primary,
      unselectedItemColor: AppColors.light2,
      selectedItemColor: AppColors.light1,
      type: BottomNavigationBarType.fixed,
      iconSize: 40,
    );
  }
}
