import 'package:app/providers/transaction.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:app/screens/transaction_detail_screen.dart';
import 'package:provider/provider.dart';

class TransactionItem extends StatelessWidget {
  final DateFormat dateFormat = DateFormat("dd/MM/yyyy HH:mm");

  @override
  Widget build(BuildContext context) {
    final transaction = Provider.of<Transaction>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 30),
      child: GridTile(
        header: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: GridTileBar(
            title: Row(
              children: [
                Text('${transaction.id}', textAlign: TextAlign.center),
                Expanded(
                  child: Text(transaction.name, textAlign: TextAlign.center),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: transaction.uploaded
                      ? Icon(Icons.cloud_done_outlined)
                      : Icon(Icons.cloud_off),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.end,
            ),
            backgroundColor: Theme.of(context).primaryColor,
          ),
        ),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              TransactionDetailScreen.routeName,
              arguments: transaction.id,
            );
          },
          child: Container(
            margin: const EdgeInsets.all(10),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Container(
                color: Theme.of(context).accentColor,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(transaction.value.toString()),
                      Text(dateFormat.format(transaction.date)),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
