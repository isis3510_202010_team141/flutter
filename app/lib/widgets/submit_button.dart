import 'package:flutter/material.dart';
//  My Imports
import '../util/consts.dart';

class SubmitButton extends StatelessWidget {
  final String text;
  final Function onTap;

  SubmitButton(this.text, this.onTap);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 55,
      child: FlatButton(
        color: Theme.of(context).accentColor,
        textColor: AppColors.tercerary,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
        onPressed: onTap,
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Text(
            text,
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ),
      ),
    );
  }
}
