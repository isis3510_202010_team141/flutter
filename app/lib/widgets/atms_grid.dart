import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:app/providers/atms.dart';
import './atm_item.dart';

class ATMsGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final atmsData = Provider.of<ATMs>(context);
    final atms = atmsData.items;
    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: atms.length,
      itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
        value: atms[i],
        child: ATMItem(),
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}
