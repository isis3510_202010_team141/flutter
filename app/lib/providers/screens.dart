//  Own
import 'package:app/providers/screen.dart';
import 'package:flutter/material.dart';
//  Screens
import '../screens/home_screen.dart';
import '../screens/transactions_overview_screen.dart';
import 'package:app/screens/statistics_screen.dart';
import 'package:app/screens/near_atm_screen.dart';

const HOME = 0;
const FINANTIAL = 1;
const STATISTICS = 2;
const ATMS = 3;

class Screens with ChangeNotifier {
  int _currentIndex = HOME;

  int get currentIndex => _currentIndex;

  final Map<int, ScreenProvider> _screens = {
    HOME: ScreenProvider(title: 'Home', child: HomeScreen()),
    FINANTIAL:
        ScreenProvider(title: 'Finantial', child: TransactionsOverviewScreen()),
    STATISTICS: ScreenProvider(title: 'Statistics', child: StatisticsScreen()),
    ATMS: ScreenProvider(title: 'Near ATMs', child: NearATMsScreen())
  };

  List<ScreenProvider> get screens => _screens.values.toList();

  ScreenProvider get currentScreen => _screens[_currentIndex];

  void setTab(int tab) {
    if (tab != currentIndex) {
      _currentIndex = tab;
      notifyListeners();
    }
  }
}
