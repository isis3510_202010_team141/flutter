//  Own
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';

enum CONNECTION { LOADING, CONNECTED, NOT_CONNECTED }

class InternetConnection with ChangeNotifier {
  //  Variable that indicates if there is connection
  CONNECTION _connection = CONNECTION.LOADING;

  //  Initial state of connection
  InternetConnection() {
    DataConnectionChecker().hasConnection.then((result) {
      if (result == true) {
        _connection = CONNECTION.CONNECTED;
      } else {
        _connection = CONNECTION.NOT_CONNECTED;
      }
      notifyListeners();
    });
  }

  //  Return _isConnected value
  CONNECTION get getConnection {
    return _connection;
  }

  //  Verifies the connection and returns a connection
  void verifyConnection() {
    DataConnectionChecker().hasConnection.then((result) {
      if (result == true) {
        _connection = CONNECTION.CONNECTED;
      } else {
        _connection = CONNECTION.NOT_CONNECTED;
      }
      notifyListeners();
    });
  }

  //  Updates connection if i is done Outside
  void updateConnection(CONNECTION newConnection) {
    _connection = newConnection;
    notifyListeners();
  }
}
