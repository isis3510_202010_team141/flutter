//  Own
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'dart:math';
//  Providers
import 'package:app/providers/transaction.dart';
import '../providers/transaction.dart';
//  SQL
import '../DAOs/transactions_dao.dart';

class Transactions with ChangeNotifier {
  List<Transaction> _transactions = [];
  ScrollController _scrollController = ScrollController();
  var _isLoadingInit = false;
  var _isLoading = false;
  var _isCreating = false;
  var _currentPage;

  /*{
    "users": "https://django-middle-finance.herokuapp.com/users/",
    "transactions": "https://django-middle-finance.herokuapp.com/transactions/",
    "tips": "https://django-middle-finance.herokuapp.com/tips/",
    "plans": "https://django-middle-finance.herokuapp.com/plans/",
    "types": "https://django-middle-finance.herokuapp.com/types/"
  }
  */

  /*
   * This function inits the Scroll with it listenmer and return it
   */
  ScrollController initScroll(int userId) {
    //  Initialize the scroll
    _scrollController.addListener(() {
      var triggerFetchMoreSize =
          0.9 * _scrollController.position.maxScrollExtent;
      if (_scrollController.position.pixels > triggerFetchMoreSize &&
          _isLoading == false) {
        //Get next
        fetchTransactionPageOfUser(userId);
      }
    });
    return _scrollController;
  }

  /** 
   * Indicates if there's loading initial transactions
   */
  bool get isLoadingInit {
    return _isLoadingInit;
  }

  /** 
   * Indicates if there's new transactions page coming in
   */
  bool get isLoading {
    return _isLoading;
  }

  /**
   * Indicates if there's new Transaction creating (POST/Catched)
   */
  bool get isCreating {
    return _isCreating;
  }

  /**
   * Returns the Items list
   */
  List<Transaction> get items {
    return [..._transactions];
  }

  /**
   * Finds a transaction By Id
   */
  Transaction findById(int id) {
    return items.firstWhere((element) => element.id == id);
  }

  /// This function fetches the current page of transactions from remote by
  /// given user
  /// Sorting by Date and newest first.
  Future<List<Transaction>> _fetchTransactionsPage(int userId) async {
    try {
      //  Fetch from Network if possible
      var url =
          'https://django-middle-finance.herokuapp.com/tr/$userId/$_currentPage/';
      const header = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      };

      final response = await http.get(url, headers: header);
      final List list = jsonDecode(response.body.toString());

      //  This assemble the transactions
      List<Transaction> dataNet = list.map(
        (e) {
          return Transaction(
            id: e['id'],
            userId: e['client'],
            name: e['name'],
            value: double.parse(e['value']),
            type: e['typeOf'] ? 1 : 0,
            date: DateTime.parse(e['date']),
            uploaded: true,
          );
        },
      ).toList();

      //  Sort to get the newest first
      dataNet.sort((a, b) {
        return b.date.compareTo(a.date);
      });

      //  Update the current page
      _currentPage = _currentPage + 1;
      notifyListeners();
      return dataNet;
    } on SocketException catch (e) {
      throw (e);
    }
  }

  ///
  ///Loads all transactions fron the caché by given user.
  /// Ordered. Newest First.
  /// Type:
  ///   0: All from cache
  ///   1: Only not saves
  ///   2. Only saved
  Future<List<Transaction>> _loadFromCache(int userId, int type) async {
    var asked = [];
    switch (type) {
      case 0:
        asked = await TransactionsDAO.getTransactions(userId);
        break;
      case 1:
        asked = await TransactionsDAO.getNotSaved(userId);
        break;
      case 2:
        asked = await TransactionsDAO.getSaved(userId);
        break;
      default:
        asked = await TransactionsDAO.getSaved(userId);
        break;
    }

    //  Given transactions:
    final askedTransactions = asked.map(
      (e) {
        return Transaction(
          id: e['id'],
          userId: e['userId'],
          name: e['name'],
          value: e['value'],
          type: e['typeOf'],
          date: DateTime.parse(e['date']),
          uploaded: e['uploaded'] == 0 ? false : true,
        );
      },
    ).toList();
    return askedTransactions;
  }

  /// This saves a given transactions to caché
  void _saveToCache(List<Transaction> transactionsToPersist) {
    transactionsToPersist.forEach((transaction) {
      //  Catching
      TransactionsDAO.insertTransaction({
        'id': transaction.id,
        'userId': transaction.userId,
        'name': transaction.name,
        'type': transaction.type,
        'value': transaction.value,
        'date': transaction.date.toString(),
        'uploaded': transaction.uploaded ? 1 : 0
      });
    });
  }

  /// This function saves a transaction remotelly
  /// If there's an error it throw it
  Future<Transaction> _saveToRemote(Transaction transaction) async {
    //  Remote URL and fetch config
    const url = 'http://django-middle-finance.herokuapp.com/transactions/';
    const header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };

    try {
      //  Creating remote
      final response = await http.post(
        url,
        headers: header,
        body: json.encode(
          {
            'client': transaction.userId,
            'name': transaction.name,
            'value': transaction.value,
            'typeOf': transaction.type == 1,
            'date': transaction.date.toString()
          },
        ),
      );

      //  Creating response - With new ID
      final _data = jsonDecode(response.body.toString());

      //  Creating the Transaction
      final createdTransaction = Transaction(
        id: _data['id'],
        userId: _data['client'],
        name: _data['name'],
        value: double.parse(_data['value']),
        type: _data['typeOf'] ? 1 : 0,
        date: DateTime.parse(_data['date']),
        uploaded: true,
      );
      return createdTransaction;
    } catch (error) {
      throw (error);
    }
  }

  /// This function:
  /// According the UDF diagram, this works in 6 cases:
  /// Case 0: There's Internet connection and it isn't something on Cache, so get
  ///   from remote and update cache
  /// Case 1: There's Internet connection, it is something in cache and exists
  ///   unsaved transactions on cache then it upload it to remote, removed saved
  ///   and excecute this method again
  /// Case 2: There's Internet connection, it is something in cache and not exists
  ///   unsaved transactions on cache, so get from remote and update cache
  /// Case 3: There isn't Internet connction, there's nothing in memory and it
  ///   isn't something on Cache,
  ///   so, this is an eventual connectivity case that will be solved on UI.
  ///   Return Internet trouble.
  /// Case 4: There isn't Internet connction, there's nothing in memory and it
  ///   is something in cache then load that data to memory.
  ///   Return Internet trouble.
  /// Case 5: There isn't Internet connction and there's something in memory.
  ///   Return Internet trouble.
  Future<bool> initialFetchTransactionsOfUser(int userId) async {
    //  Init as Loading transactions
    _isLoadingInit = true;

    //  Set Initial Date
    _currentPage = 1;

    /**
     * Get Transactions from Remote
     * Internet?
     * [Y] Multi-thread
     * [Y] Eventual connectivity
     * [Y] Catching strategies
     */
    return _fetchTransactionsPage(userId).then((dataNet) async {
      //  Internet [Y]

      //  Get the transaction from Caché
      final dataCatch = await _loadFromCache(userId, 0);

      //  There's something on cache?
      if (dataCatch.isEmpty) {
        //  There's something on cache? [N]

        //  Updating on caché
        _saveToCache(dataNet);

        //  Uploading in memory
        _transactions = dataNet;
        //  Case 0 [READY]
        notifyListeners();
      } else {
        //  There's something on cache? [Y]

        //  Getting unsaved transactions
        final unsavedTransactions = await _loadFromCache(userId, 1);

        //  There's unsaved transactions?
        if (unsavedTransactions.isEmpty) {
          //  There's unsaved transactions? [N]

          //  Delete all in caché of that user
          TransactionsDAO.removeAll(userId);

          //  Updating on caché of that user
          _saveToCache(dataNet);

          //  Uploading in memory
          _transactions = dataNet;
          //  Case 1 [READY]
          notifyListeners();
        } else {
          //  There's unsaved transactions? [Y]

          //  Get from cache not saved
          final dataCatchAndNotSaved = await _loadFromCache(userId, 1);
          //  and remove from cache that are not saved
          await TransactionsDAO.removeNotSaved(userId);
          //  Upload to Network not saved
          dataCatchAndNotSaved.forEach((trans) {
            addTransaction(trans);
          });
          //  Remove from cache that are saved
          await TransactionsDAO.removeSaved(userId);
          //  Finally reload
          initialFetchTransactionsOfUser(userId);
          //  Case 2 [READY]
        }
      }
      //  Init as Loading transactions
      _isLoadingInit = false;
      return true;
    }).catchError((error) async {
      //  Internet [N]

      //  Is something on memory?
      if (_transactions.isEmpty) {
        //  Is something on memory? [N]

        //  Get the transaction from Caché
        final dataCatch = await _loadFromCache(userId, 0);

        //  Is it somethig on cache?
        if (dataCatch.isNotEmpty) {
          //  Is it somethig on cache? [Y]

          //  Eventual connectivity case, transactions are catched
          _transactions = dataCatch;
          //  Case 4 [READY]
        }
        //  Else {
        //    Is it somethig on cache? [N]
        //    Eventual connectivity case, transactions list stay empty
        //    Case 3 [READY]
        //  }

      }
      //  Else {
      //    Is something on memory? [Y]
      //    Eventual connectivity case, transactions list stay with its data
      //    Case 5 [READY]
      //  }

      //  Init as Loading transactions
      _isLoadingInit = false;
      return false;
    });
  }

  /**
   * ===========================================================================
   */
  void fetchTransactionPageOfUser(int userId) async {
    _isLoading = true;
    notifyListeners();

    await _fetchTransactionsPage(userId).then((dataNet) {
      //  Updating on caché
      dataNet.forEach((element) {
        _transactions.add(element);
      });

      //  Finish to load
      _isLoading = false;

      //  Notify
      notifyListeners();
    }).catchError((error) {
      //  Finish to load
      _isLoading = false;

      //  Notify
      notifyListeners();
    });
  }

  /// This function add a new transaction to the user transactions
  /// It depends on if there's internet or not it have cases too:
  ///   Case 1: There's Internet
  ///   Case 2: There's not Internet
  Future<void> addTransaction(Transaction tr) async {
    //  Adding
    _isCreating = true;
    notifyListeners();

    try {
      //  Saving remote
      List<Transaction> saved = [];
      saved.add(await _saveToRemote(tr));

      //  Catching
      _saveToCache(saved);

      _transactions.insert(0, saved[0]);

      //  Added
      _isCreating = false;
      //  Case 1 [READY]
      notifyListeners();
    } on SocketException catch (_) {
      var rndm = Random();
      //  If there's no connection

      List<Transaction> saved = [];
      var transaction = Transaction(
        id: rndm.nextInt(100000000),
        userId: tr.userId,
        name: tr.name,
        value: tr.value,
        type: tr.type,
        date: tr.date,
        uploaded: false,
      );
      saved.add(transaction);

      //  Catching
      _saveToCache(saved);

      //  Adding to our list
      _transactions.insert(0, saved[0]);

      //  Added
      _isCreating = false;

      //  Case 2 [READY]
      notifyListeners();
    } catch (error) {
      print('Error');
      throw (error);
    }
  }
}
