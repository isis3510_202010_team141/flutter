import 'package:flutter/cupertino.dart';

class User with ChangeNotifier {
  final int id;
  final String name;
  final int phone;
  final DateTime birthDate;
  final String email;

  User({
    @required this.id,
    @required this.name,
    @required this.phone,
    @required this.email,
    @required this.birthDate,
  });
}
