import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:location/location.dart';

import './atm.dart';

class ATMs with ChangeNotifier {
  ATMs() {
    getNearATMs();
  }

  List<ATM> _items = [];

  List<ATM> get items {
    return _items;
  }

  Future<Map<String, Object>> _getCurrentUserLocation() async {
    final locData = await Location().getLocation();
    return {
      'coordinates': {'lat': locData.latitude, 'lng': locData.longitude}
    };
  }

  void getNearATMs() {
    const url = 'https://express-finance.herokuapp.com/app/locations';
    _getCurrentUserLocation().then((coords) {
      http.post(url,
          body: json.encode(coords),
          headers: {'Content-Type': 'application/json'}).then((response) {
        List resp = json.decode(response.body) as List;
        _items = resp
            .map((e) => ATM(name: e['name'], address: e['address']))
            .toList();
        notifyListeners();
      });
    });
  }
}
