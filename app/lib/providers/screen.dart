//  Own
import 'package:flutter/material.dart';

class ScreenProvider with ChangeNotifier {
  final String title;

  final Widget child;

  ScreenProvider({
    @required this.title,
    @required this.child,
  });
}
