import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expyryDate;
  int _userId;
  String _userName;
  String _userAge;
  String _userPhone;
  String _userEmail;

  var prefs;

  var responseData;

  bool get isAuth {
    return token != null;
  }

  String get token {
    if (_expyryDate != null &&
        _expyryDate.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  Future<void> signUp(
    String email,
    String password,
    String name,
    String age,
    String phone,
  ) async {
    const url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCDn1uJ2vmU0Yy5bf7nAO3ru23KgA7T758';

    const urlEmail = 'https://django-middle-finance.herokuapp.com/users/';
    const header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };

    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      final responseData = json.decode(response.body);
      if (responseData['error'] != null)
        throw Exception(responseData['error']['message']);
      _token = responseData['idToken'];
      //  Looking for the User ID

      var bod = json.encode({
        'name': name,
        'age': age,
        'phone': phone,
        'email': email,
      });
      var user = json.decode((await http.post(
        urlEmail,
        headers: header,
        body: bod,
      ))
          .body);
      _userId = user['id'];
      _userName = user['name'];
      _userAge = '${user['age']}';
      _userPhone = '${user['phone']}';
      _userEmail = user['email'];

      _expyryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseData['expiresIn'],
          ),
        ),
      );
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> signIn(String email, String password) async {
    const url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCDn1uJ2vmU0Yy5bf7nAO3ru23KgA7T758';

    final response = await http.post(
      url,
      body: json.encode(
        {
          'email': email,
          'password': password,
          'returnSecureToken': true,
        },
      ),
    );
    final responseData = json.decode(response.body);
    _token = responseData['idToken'];
    //  Looking for the User ID

    var urlEmail = 'http://django-middle-finance.herokuapp.com/client/$email';
    const header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    var user = json.decode((await http.get(urlEmail, headers: header)).body);
    _userId = user['id'];
    _userName = user['name'];
    _userAge = '${user['age']}';
    _userPhone = '${user['phone']}';
    _userEmail = user['email'];
    _expyryDate = DateTime.now().add(
      Duration(
        seconds: int.parse(
          responseData['expiresIn'],
        ),
      ),
    );
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    final userData = json.encode(
      {
        'token': _token,
        'userId': _userId,
        'userName': _userName,
        'userAge': _userAge,
        'userEmail': _userEmail,
        'userPhone': _userPhone,
        'expiryDate': _expyryDate.toIso8601String(),
      },
    );
    prefs.setString('userData', userData);
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _userName = extractedUserData['userName'];
    _userAge = extractedUserData['userAge'];
    _userEmail = extractedUserData['userEmail'];
    _userPhone = extractedUserData['userPhone'];
    _expyryDate = expiryDate;
    notifyListeners();
    return true;
  }

  int get getUserID => _userId;
  String get getUserName => _userName;
  String get getUserAge => _userAge;
  String get getUserEmail => _userEmail;
  String get getUserPhone => _userPhone;

  void logout() async {
    _token = null;
    _userId = null;
    _expyryDate = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    final userData = json.encode(
      {
        'token': null,
        'userId': null,
        'userName': null,
        'userAge': null,
        'userEmail': null,
        'userPhone': null,
        'expiryDate': null,
      },
    );
    prefs.setString('userData', userData);
  }
}
