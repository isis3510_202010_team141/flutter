import 'package:flutter/material.dart';

class Transaction with ChangeNotifier {
  final int id;
  final int userId;
  final String name;
  final int type;
  final double value;
  final DateTime date;
  final bool uploaded;

  Transaction({
    @required this.id,
    @required this.userId,
    @required this.name,
    @required this.type,
    @required this.value,
    @required this.date,
    @required this.uploaded,
  });
}
