//  Own
import 'package:flutter/material.dart';

class ATM with ChangeNotifier {
  final String name;
  final String photo;
  final String address;

  ATM(
      {@required this.name,
      this.photo =
          'https://www.iamexpat.nl/sites/default/files/styles/article--full/public/banking-netherlands_0.jpg',
      @required this.address});
}
