import 'package:flutter/material.dart';

class AppColors {
  static MaterialColor get primary {
    Map<int, Color> colorCodesPrimary = {
      50: Color.fromRGBO(36, 35, 38, .1),
      100: Color.fromRGBO(36, 35, 38, .2),
      200: Color.fromRGBO(36, 35, 38, .3),
      300: Color.fromRGBO(36, 35, 38, .4),
      400: Color.fromRGBO(36, 35, 38, .5),
      500: Color.fromRGBO(36, 35, 38, .6),
      600: Color.fromRGBO(36, 35, 38, .7),
      700: Color.fromRGBO(36, 35, 38, .8),
      800: Color.fromRGBO(36, 35, 38, .9),
      900: Color.fromRGBO(36, 35, 38, 1),
    };
    return MaterialColor(0xFF242326, colorCodesPrimary);
  }

  static Color get secondary => Color(0xFF36363B);
  static Color get tercerary => Color(0xFF495F73);
  static MaterialColor get light1 {
    Map<int, Color> colorCodesAccent = {
      50: Color.fromRGBO(212, 212, 212, .1),
      100: Color.fromRGBO(212, 212, 212, .2),
      200: Color.fromRGBO(212, 212, 212, .3),
      300: Color.fromRGBO(212, 212, 212, .4),
      400: Color.fromRGBO(212, 212, 212, .5),
      500: Color.fromRGBO(212, 212, 212, .6),
      600: Color.fromRGBO(212, 212, 212, .7),
      700: Color.fromRGBO(212, 212, 212, .8),
      800: Color.fromRGBO(212, 212, 212, .9),
      900: Color.fromRGBO(212, 212, 212, 1),
    };
    return MaterialColor(0xFFD4D4D4, colorCodesAccent);
  }

  static Color get light2 => Color(0xFFA0D2FF);
  static Color get income => Color(0xFF1FC34D);
  static Color get outcome => Color(0xFFD44C4C);
  static Color get facebook => Color(0xFF4268B3);
}
