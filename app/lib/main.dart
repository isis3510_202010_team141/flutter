//  Own
import 'package:app/screens/app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//  Const
import 'package:app/util/consts.dart';
//  Screens
import './screens/transactions_overview_screen.dart';
import 'package:app/screens/transaction_detail_screen.dart';
import 'package:app/screens/new_transactions.dart';
import 'package:app/screens/auth_screen.dart';
//  Providers
import 'package:app/providers/internetConnection.dart';
import 'package:app/providers/transactions.dart';
import 'package:app/providers/atms.dart';
import 'package:app/providers/auth.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        //  This is the Provider of internet
        ChangeNotifierProvider.value(value: InternetConnection()),
        //  This is the Auth provider. Manages all about the Auth data.
        ChangeNotifierProvider.value(value: Auth()),
        //  This manages all te transactions Data.
        ChangeNotifierProvider.value(value: Transactions()),
        //  This manages all the ATMS service.
        ChangeNotifierProvider.value(value: ATMs()),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Personal Finances',
          theme: ThemeData(
            primarySwatch: AppColors.primary,
            accentColor: AppColors.light1,
            fontFamily: 'NunitoSans',
            unselectedWidgetColor: AppColors.light1,
          ),
          home: auth.isAuth
              ? FinancesApp()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? Center(
                              child: CircularProgressIndicator(),
                            )
                          : AuthScreen(),
                ),
          routes: {
            TransactionsOverviewScreen.routeName: (ctx) =>
                TransactionsOverviewScreen(),
            TransactionDetailScreen.routeName: (ctx) =>
                TransactionDetailScreen(),
            NewTransactionScreen.routeName: (ctx) => NewTransactionScreen(),
          },
        ),
      ),
    );
  }
}
